#include <iostream>
#include "algorithms.hpp"

int main(int argc, char **argv) {
	std::string fileName, algorithm = "CARLIER";
	
	if (argc != 3) {
		fileName = "/home/damian/projects/SPD/carlier/testfiles/CARLIER/in/SCHRAGE3.DAT";
		algorithm = "CARLIER";
	} else {
		fileName = argv[1];
		algorithm = argv[2];
	}

	std::vector<Task> tasks = loadTasks(fileName);
	
	
	if (algorithm == "CARLIER") {
		auto UB = std::numeric_limits<Task::value_type>::max();
		auto ret = carlier(tasks, UB);
		std::cout << ret << std::endl;
	} else if (algorithm == "SCHRAGE") {
		auto ret = schrage(tasks);
		std::cout << ret << std::endl;
	} else if (algorithm == "SCHRAGE_PRE") {
		auto ret = schrage_pre(tasks);
		std::cout << ret << std::endl;
	}
	
	return 0;
}
