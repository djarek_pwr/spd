#ifndef TASK_H
#define TASK_H
#include <vector>
#include <fstream>

uint32_t lastId = 0;

template <typename TimeType>
struct TaskStruct
{
	TaskStruct() {
		id = lastId++;
	}
	uint32_t id = 0;
	TimeType r = 0, p = 0, q = 0;
	typedef TimeType value_type;
};

template <typename TimeType>
std::istream& operator>>(std::istream& input, TaskStruct<TimeType>& task) {
	input >> task.r;
	input >> task.p;
	input >> task.q;
	return input;
}

typedef TaskStruct<uint64_t> Task;
typedef std::vector<Task> TaskVec;

template <typename Type >
struct DefaultLoader
{
	void operator()(std::istream& input, Type& variable) const {
		input >> variable;
	}
};

template <typename Type, typename Callable = DefaultLoader<Type>>
void readAndCheck(std::istream& input, Type& variable, const Callable loader = Callable {})
{
	loader(input, variable);
	
	if (!input.good() && !input.eof()) {
		throw;
	}
}

template <typename TaskType = Task, typename Callable = DefaultLoader<TaskType>>
std::vector<TaskType> loadTasks(const std::string& fileName, const Callable taskLoader = Callable {})
{
	std::vector<TaskType> tasks;
	std::ifstream inputFile {fileName};
	
	if (!inputFile.is_open()) {
		throw;
	}
	
	auto taskNumber = 0ull;
	readAndCheck(inputFile, taskNumber);
	tasks.reserve(taskNumber);
	
	for (auto i = 0ull; i < taskNumber; ++i) {
		TaskType task;
		readAndCheck(inputFile, task, taskLoader);
		tasks.emplace_back(task);
	}

	return tasks;
}

#endif //TASK_H