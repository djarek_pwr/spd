#ifndef ALGORITHMS_H
#define ALGORITHMS_H
#include "task.hpp"
#include <algorithm>
#include <set>
#include <queue>

template <typename TaskType>
struct CompareByQ
{
	bool operator()(const TaskType& first, const TaskType& second ) {
		return first.q < second.q;
	}
};

template <typename TaskType>
struct CompareByR
{
	bool operator()(const TaskType& first, const TaskType& second ) {
		return first.r < second.r;
	}
};

template <typename TaskType>
auto schrage(std::vector<TaskType>& tasks)
{
	typedef typename TaskType::value_type TimeType;
	std::stable_sort(tasks.begin(), tasks.end(), [](const TaskType& first, const TaskType& second) {
		return second.r < first.r;
	});
	std::vector<TaskType> taskPermutation;
	taskPermutation.reserve(tasks.size());
	TimeType t {tasks.back().r};
	TimeType Cmax {0};
	std::set<TaskType, CompareByQ<TaskType>> readyTasks;
	while (!readyTasks.empty() || !tasks.empty()) {
		auto min_r = tasks.back().r;
		while (!tasks.empty() && min_r <= t) {
			readyTasks.insert(std::move(tasks.back()));
			tasks.pop_back();
			min_r = tasks.back().r;
		}
		if (readyTasks.empty()) {
			t = min_r;
			continue;
		}
		auto taskIt = --readyTasks.end();
		t += taskIt->p;
		Cmax = std::max(Cmax, t + taskIt->q);
		taskPermutation.emplace_back(*taskIt);
		readyTasks.erase(taskIt);
	}
	
	tasks = taskPermutation;
	
	return Cmax;
}

template <typename TaskType>
using PriorityQueue = std::priority_queue<TaskType, std::deque<TaskType>, CompareByQ<TaskType>>;

template <typename TaskType>
auto schrage_pre(std::vector<TaskType>& tasks)
{
	typedef typename TaskType::value_type TimeType;
	std::stable_sort(tasks.begin(), tasks.end(), [](const TaskType& first, const TaskType& second) {
		return second.r < first.r;
	});

	std::vector<TaskType> taskPermutation;
	taskPermutation.reserve(tasks.size());

	TimeType t {tasks.back().r};
	TimeType Cmax {0};
	//std::set<TaskType,CompareByQ<TaskType>> readyTasks;
	PriorityQueue<TaskType> readyTasks;
	TaskType runningTask;

	tasks.back().q = std::numeric_limits<TimeType>::max();
	while (!readyTasks.empty() || !tasks.empty()) {
		auto min_r = tasks.back().r;
		while (!tasks.empty() && min_r <= t) {
			auto e = std::move(tasks.back());
			tasks.pop_back();
			min_r = tasks.back().r;
			if (e.q > runningTask.q) {
				runningTask.p = t - e.r;
				t = e.r;
				if (runningTask.p > 0) {
					//readyTasks.insert(std::move(runningTask));
					readyTasks.emplace(std::move(runningTask));
				}
			}
			//readyTasks.insert(std::move(e));
			readyTasks.emplace(std::move(e));
		}
		if (readyTasks.empty()) {
			t = min_r;
			continue;
		}
		auto& task = readyTasks.top();
		t += task.p;
		Cmax = std::max(Cmax, t + task.q);
		taskPermutation.push_back(task);
		runningTask = task;
		readyTasks.pop();
	}
	
	tasks = taskPermutation;
	return Cmax;
}

template <typename TaskType>
auto findCAndB(std::vector<TaskType>& inputTasks, typename TaskType::value_type Cmax) {
	
	typename TaskType::value_type sum = 0;

	auto b = inputTasks.begin(), it = inputTasks.begin();

	for(; it != inputTasks.end(); ++it) {
		if(sum < it->r) {
			sum = it->r;
		}
		sum += it->p;
		
		if (sum + it->q == Cmax) {
			b = it;
		}
	}
	sum = 0;
	

	auto q = b->q;
	it = inputTasks.begin();
	for (; it <= b; ++it) {
		sum = 0;

		for (auto it2 = it; it2 <= b; ++it2) {
			sum += it->p;
		}
		if (q + it->r + sum == Cmax) {
			break;
		}
	}
	auto a = it;
	
	auto c = std::find_if(a, b+1, [b](const TaskType& task) {
		return task.q < b->q;
	});
	
	return std::make_pair(c, b);
}

template <typename TaskType>
auto carlier(std::vector<TaskType>& inputTasks, typename TaskType::value_type& UB, typename TaskType::value_type lastU = 0)
{
	typedef typename TaskType::value_type TimeType;

	auto U = schrage(inputTasks);

	if (U < UB) {
		UB = U;
	}

	auto pair = findCAndB(inputTasks, U);
	auto c = pair.first;
	auto b = pair.second;

	if (c == inputTasks.end()) {
		return UB;
	}
	
	auto rPrim = std::min_element(c + 1, inputTasks.end(), CompareByR<TaskType>())->r;
	auto qPrim = std::min_element(c + 1, inputTasks.end(), CompareByQ<TaskType>())->q;
	TimeType pPrim = 0;
	for (auto it = c + 1; it <= b; ++it) {
		pPrim += it->p;
	}
	
	auto oldC = *c;
	c->r = std::max(c->r, rPrim + pPrim);
	
	auto L = schrage_pre(inputTasks);
	if (L < UB) {
		carlier(inputTasks, UB, U);
	}
	
	auto cmp = [&oldC](const auto& task) {
		return task.id == oldC.id;
	};
	
	c = std::find_if(inputTasks.begin(), inputTasks.end(), cmp);
	
	c->r = oldC.r;
	
	oldC = *c;
	c->q = std::max(c->q, qPrim + rPrim);
	L = schrage_pre(inputTasks);
	if (L < UB) {
		carlier(inputTasks, UB, U);
	}
	
	c = std::find_if(inputTasks.begin(), inputTasks.end(), cmp);

	c->q = oldC.q;
	
	return UB;
}

#endif //ALGORITHMS_H
