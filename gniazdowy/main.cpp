#include <fstream>
#include <vector>
#include <deque>
#include <algorithm>
#include <iostream>

struct DependencyCycleException : public std::exception {
	const char* what() const noexcept override {
		return "Dependency cycle detected.";
	}
};

struct TaskTime {
	int32_t start = -1;
	int32_t end = -1;
};

struct Task {
	int32_t nextTechTask = -1;
	int32_t nextMachineTask = -1;
	int32_t duration = 0;
	int32_t machineIndex = 0;
	int32_t previousTaskCount = 0;
	int32_t index = -1;
};

auto loadTasks(const std::string& fileName)
{
	std::vector<int32_t> permutations;
	std::vector<Task> tasks;
	
	std::ifstream inputFile (fileName);
	
	int32_t taskCount = 0;
	int32_t machineCount = 0;
	inputFile >> taskCount;
	
	tasks.resize(taskCount);
	
	for (int32_t i = 0; i < taskCount; ++i) {
		auto& nextTechTask = tasks[i].nextTechTask;
		inputFile >> nextTechTask;
		if (--nextTechTask >= 0) {
			++tasks[nextTechTask].previousTaskCount;
		}
		tasks[i].index = i;
	}
	
	for (int32_t i = 0; i < taskCount; ++i) {
		auto& nextMachineTask = tasks[i].nextMachineTask;
		inputFile >> nextMachineTask;

		if (--nextMachineTask >= 0) {
			++tasks[nextMachineTask].previousTaskCount;
		}
	}
	
	for (int32_t i = 0; i < taskCount; ++i) {
		inputFile >> tasks[i].duration;
		if (tasks[i].duration == 5) {
			auto& task = tasks[i];
			std::cout << task.index << std::endl;
		}
	}
	
	inputFile >> machineCount;
	
	
	for (int32_t i = 0; i < machineCount; ++i) {
		int32_t taskId = 0;
		inputFile >> taskId;
		while (taskId != 0) {
			tasks[--taskId].machineIndex = i;
			inputFile >> taskId;
		}
	}

	if (!inputFile.good()) {
		throw;
	}

	return std::make_pair(std::move(tasks), machineCount);
}

auto socketProblemAlgorithm(std::vector<Task>& tasks, int32_t machineCount) {
	std::deque<const Task*> readyTasks;
	std::vector<int32_t> machineTimes (machineCount, 0);
	std::vector<TaskTime> taskTimes;
	
	taskTimes.resize(tasks.size());
	
	for (auto& task : tasks) {
		if (task.previousTaskCount == 0) {
			readyTasks.push_back(&task);
			taskTimes[task.index].start = 0;
		}
	}

	uint32_t tasksExecuted = 0;
	while (!readyTasks.empty()) {
		auto task = readyTasks.front();
		readyTasks.pop_front();
		++tasksExecuted;

		auto& machineTime = machineTimes[task->machineIndex];
		machineTime = std::max(machineTime, taskTimes[task->index].start);
		taskTimes[task->index].start = machineTime;
		machineTime  += task->duration;
		taskTimes[task->index].end = machineTime;

		if (task->nextMachineTask >= 0) {
			--tasks[task->nextMachineTask].previousTaskCount;
			taskTimes[task->nextMachineTask].start = std::max(machineTime, taskTimes[task->nextMachineTask].start);
			if (tasks[task->nextMachineTask].previousTaskCount == 0) {
				readyTasks.push_back(&tasks[task->nextMachineTask]);
				
			}
		}

		if (task->nextTechTask >= 0) {
			--tasks[task->nextTechTask].previousTaskCount;
			taskTimes[task->nextTechTask].start = std::max(machineTimes[tasks[task->nextTechTask].machineIndex], machineTime);
			if (tasks[task->nextTechTask].previousTaskCount == 0) {
				readyTasks.push_back(&tasks[task->nextTechTask]);
			}
		}

	}
	
	if (tasksExecuted != tasks.size()) {
		throw DependencyCycleException();
	}

	return taskTimes;
}

int main(int argc, char** argv) {
	auto pair = loadTasks("data0.txt");
	std::vector<TaskTime> ret;
	try {
		ret = socketProblemAlgorithm(pair.first, pair.second);
	} catch (DependencyCycleException& e) {
		std::cout << "An error has occurred: " << e.what() << std::endl;
		return 0;
	}
	
	int32_t CMax = 0;
	
	for (const auto& taskTime : ret) {
		std::cout << taskTime.start <<" " << taskTime.end <<  std::endl;
		CMax = std::max(CMax, taskTime.end);
	}
	
	std::cout << CMax << std::endl;
	return 0;
}
