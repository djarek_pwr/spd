#include "task.h"
#include <iostream>

TimeType neh(TaskVector& tasks);

Task loadTask(std::ifstream& inputFile, uint32_t machineCount)
{
	Task ret;
	ret.machineTimes.resize(machineCount);
	TimeType sum = 0;

	for (auto& time : ret.machineTimes) {
		inputFile >> time;
		sum += time;
		
		if (!inputFile.good()) {
			throw;
		}
	}
	ret.sum = sum;
	
	return ret;
}

auto loadTasks(const std::string& fileName)
{
	TaskVector tasks;
	std::ifstream inputFile {fileName};
	
	if (!inputFile.is_open()) {
		throw;
	}
	
	auto taskCount = 0u;
	inputFile >> taskCount;
	tasks.reserve(taskCount);

	auto machineCount = 0u;
	inputFile >> machineCount;
	
	for (auto i = 0u; i < taskCount; ++i) {
		tasks.emplace_back(loadTask(inputFile, machineCount));
		if (!inputFile.good()) {
			throw;
		}
	}

	return tasks;
}

int main(int argc, char** argv)
{
	if (argc != 2) {
		std::cout << "Arg count != 2." << std::endl;
		return 0;
	}
	
	auto tasks = loadTasks(argv[1]);
	auto CMax = neh(tasks);
	
	std::cout << "CMax: " << CMax << std::endl;
	
	return 0;
}