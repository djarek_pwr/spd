#ifndef TASK_H
#define TASK_H

#include <vector>
#include <fstream>

typedef uint32_t TimeType;

struct Task {
	std::vector<TimeType> machineTimes;
	TimeType sum;
};

typedef std::vector<Task> TaskVector;


#endif //TASK_H