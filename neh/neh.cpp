#include "task.h"
#include <algorithm>

TimeType calculateCMax(const TaskVector& tasks, std::vector<uint32_t>& machineTimes) 
{
	std::fill(machineTimes.begin(), machineTimes.end(), 0);

	TimeType previousMachineTime = 0;
	auto machineTimesIt = machineTimes.begin();

	for (const auto& time : tasks.front().machineTimes) {
		*machineTimesIt = previousMachineTime + time;
		previousMachineTime = *machineTimesIt;
		machineTimesIt++;
	}

	for (auto taskIt = tasks.begin() + 1; taskIt != tasks.end(); taskIt++) {
		previousMachineTime = machineTimes.front();
		machineTimesIt = machineTimes.begin();
		for (const auto& time : taskIt->machineTimes) {
			
			auto& currentMachineTime = *machineTimesIt;

			currentMachineTime = std::max(previousMachineTime, currentMachineTime) + time;

			previousMachineTime = currentMachineTime;
			machineTimesIt++;
		}
		
	}
	
	return machineTimes.back();
}

TimeType neh(TaskVector& tasks)
{
	std::stable_sort(tasks.begin(), tasks.end(), [](const auto& lhs, const auto& rhs) {
		return lhs.sum >= rhs.sum;
	});

	std::vector<uint32_t> machineTimes (tasks.front().machineTimes.size(), 0);

	TaskVector permutation;
	permutation.reserve(tasks.size());

	permutation.emplace_back(tasks.front());
	tasks.erase(tasks.begin());

	permutation.emplace_back(tasks.front());
	tasks.erase(tasks.begin());

	TimeType newCMax = calculateCMax(permutation, machineTimes);
	auto bestCMax = newCMax;

	std::swap(permutation.front(), permutation.back());
	newCMax = calculateCMax(permutation, machineTimes);
	
	if (newCMax > bestCMax) {
		std::swap(permutation.front(), permutation.back());
	}
	
	
	for (auto taskIt = tasks.begin(); taskIt != tasks.end();) {
		permutation.emplace_back(*taskIt);
		taskIt = tasks.erase(tasks.begin());

		newCMax = 0;
		bestCMax = 0;

		auto bestPositionIt = permutation.end();

		newCMax = calculateCMax(permutation, machineTimes);
		bestCMax = newCMax;
		
		for (auto permutationIt = permutation.end()-1; permutationIt != permutation.begin(); --permutationIt) {
			auto nextPosition = permutationIt - 1;
			std::swap(*permutationIt, *nextPosition);
			newCMax = calculateCMax(permutation, machineTimes);
			
			if (newCMax < bestCMax) {
				bestPositionIt = nextPosition;
				bestCMax = newCMax;
			}
		}

		//The task is at begin(), move elements so that the task being tested is put at the best position
		if (bestPositionIt != permutation.begin()) {
			auto endIt =  bestPositionIt == permutation.end() ? bestPositionIt : bestPositionIt + 1;
			std::rotate(permutation.begin(), permutation.begin() + 1, endIt);
		}
	}

	return calculateCMax(permutation, machineTimes);
}