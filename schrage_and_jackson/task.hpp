#ifndef TASK_H
#define TASK_H
#include <vector>
#include <fstream>

template <typename TimeType>
struct TaskStruct
{
	TimeType r, p, q;
	typedef TimeType value_type;
};

template <typename TimeType>
std::istream& operator>>(std::istream& input, TaskStruct<TimeType>& task) {
	input >> task.r;
	input >> task.p;
	input >> task.q;
	return input;
}

typedef TaskStruct<uint64_t> Task;
typedef std::vector<Task> TaskVec;

template <typename Type >
struct DefaultLoader
{
	void operator()(std::istream& input, Type& variable) const {
		input >> variable;
	}
};

template <typename Type, typename Callable = DefaultLoader<Type>>
void readAndCheck(std::istream& input, Type& variable, const Callable loader = Callable {})
{
	loader(input, variable);
	
	if (!input.good() && !input.eof()) {
		throw;
	}
}

template <typename TaskType = Task, typename Callable = DefaultLoader<TaskType>>
std::vector<TaskType> loadTasks(const std::string& fileName, const Callable taskLoader = Callable {})
{
	std::vector<TaskType> tasks;
	std::ifstream inputFile {fileName};
	
	if (!inputFile.is_open()) {
		throw;
	}
	
	auto taskNumber = 0ull;
	readAndCheck(inputFile, taskNumber);
	tasks.reserve(taskNumber);
	
	TaskType task;
	for (auto i = 0ull; i < taskNumber; ++i) {
		readAndCheck(inputFile, task, taskLoader);
		tasks.emplace_back(task);
	}

	return tasks;
}

#endif //TASK_H