#ifndef ALGORITHMS_H
#define ALGORITHMS_H
#include "task.hpp"
#include <algorithm>
#include <set>
#include <queue>

template <typename TaskType>
struct RetType
{
	typedef typename TaskType::value_type TimeType;
	std::vector<TaskType> taskPermutation;
	TimeType Cmax;
	RetType() = default;
	RetType (std::vector<TaskType>&& taskPermutation, TimeType&& Cmax):
		taskPermutation(taskPermutation),
		Cmax(Cmax) {}
};

template <typename TaskType>
struct CompareByQ
{
	bool operator()(const TaskType& first, const TaskType& second ) {
		return first.q < second.q;
	}
};

template <typename TaskType>
struct CompareByR
{
	bool operator()(const TaskType& first, const TaskType& second ) {
		return first.r < second.r;
	}
};

template <typename TaskType>
RetType<TaskType> jackson(const std::vector<TaskType>& tasks)
{
	typedef typename TaskType::value_type TimeType;
	auto taskPermutation = tasks;
	std::sort(taskPermutation.begin(), taskPermutation.end(), [](const TaskType& first, const TaskType& second) {
		return second.r > first.r;
	});

	auto currentTime = TimeType {0};
	auto lastP = TimeType {0};
	for (const auto& task : taskPermutation) {
		currentTime = std::max(task.r, currentTime + lastP);
		lastP = task.p;
	}
	currentTime += lastP;
	return RetType<TaskType>(std::move(taskPermutation), std::move(currentTime));
}

template <typename TaskType>
RetType<TaskType> schrage(const std::vector<TaskType>& inputTasks)
{
	auto tasks = inputTasks;
	typedef typename TaskType::value_type TimeType;
	std::sort(tasks.begin(), tasks.end(), [](const TaskType& first, const TaskType& second) {
		return second.r < first.r;
	});
	
	std::vector<TaskType> taskPermutation;
	taskPermutation.reserve(tasks.size());
	TimeType t {tasks.back().r};
	TimeType Cmax {0};
	std::set<TaskType, CompareByQ<TaskType>> readyTasks;
	while (!readyTasks.empty() || !tasks.empty()) {
		auto min_r = tasks.back().r;
		while (!tasks.empty() && min_r <= t) {
			readyTasks.insert(std::move(tasks.back()));
			tasks.pop_back();
			min_r = tasks.back().r;
		}
		if (readyTasks.empty()) {
			t = min_r;
			continue;
		}
		auto taskIt = --readyTasks.end();
		t += taskIt->p;
		Cmax = std::max(Cmax, t + taskIt->q);
		taskPermutation.emplace_back(*taskIt);
		readyTasks.erase(taskIt);
	}
	return RetType<TaskType> {std::move(taskPermutation), std::move(Cmax)};
}

template <typename TaskType>
RetType<TaskType> schrage_pre(const std::vector<TaskType>& inputTasks)
{
	auto tasks = inputTasks;
	typedef typename TaskType::value_type TimeType;
	std::sort(tasks.begin(), tasks.end(), [](const TaskType& first, const TaskType& second) {
		return second.r < first.r;
	});
	
	std::vector<TaskType> taskPermutation;
	taskPermutation.reserve(tasks.size());
	TimeType t {tasks.back().r};
	TimeType Cmax {0};
	std::set<TaskType,CompareByQ<TaskType>> readyTasks;
	TaskType runningTask {0};

	tasks.back().q = std::numeric_limits<TimeType>::max();
	while (!readyTasks.empty() || !tasks.empty()) {
		auto min_r = tasks.back().r;
		while (!tasks.empty() && min_r <= t) {
			auto e = std::move(tasks.back());
			tasks.pop_back();
			min_r = tasks.back().r;
			if (e.q > runningTask.q) {
				runningTask.p = t - e.r;
				t = e.r;
				if (runningTask.p > 0) {
					readyTasks.insert(std::move(runningTask));
				}
			}
			readyTasks.insert(std::move(e));
		}
		if (readyTasks.empty()) {
			t = min_r;
			continue;
		}
		auto taskIt = --readyTasks.end();
		t += taskIt->p;
		Cmax = std::max(Cmax, t + taskIt->q);
		taskPermutation.push_back(*taskIt);
		runningTask = *taskIt;
		readyTasks.erase(taskIt);
	}
	return RetType<TaskType> {std::move(taskPermutation), std::move(Cmax)};
}

template <typename TaskType>
auto findB(RetType<TaskType>& input)
{
	typename TaskType::value_type sum = 0;
	auto& inputTasks = input.taskPermutation;
	auto ret = inputTasks.begin(), it = inputTasks.begin();

	for(; it != inputTasks.end(); ++it) {
		if(sum < it->r) {
			sum = it->r;
		}
		sum += it->p;
		
		if (sum + it->q == input.Cmax) {
			ret = it;
		}
	}

	return ret;
}

template <typename TaskType>
auto findA(RetType<TaskType>& input, typename std::vector<TaskType>::iterator b)
{
	typename TaskType::value_type sum = 0;
	auto& inputTasks = input.taskPermutation;
	auto q = b->q;
	auto it = inputTasks.begin();
	for (; it <= b; ++it) {
		sum = 0;

		for (auto it2 = it; it2 <= b; ++it2) {
			sum += it->p;
		}
		if (q + it->r + sum == input.Cmax) {
			break;
		}
	}
	
	return it;
}

template <typename TaskType>
auto findC(std::vector<TaskType>& inputTasks, typename std::vector<TaskType>::iterator b, typename std::vector<TaskType>::iterator a) {
	if (b != a && b != inputTasks.end()) {
		return --b;
	} else {
		return inputTasks.end();
	}
}


#include <iostream>
template <typename TaskType>
RetType<TaskType> carlier(const std::vector<TaskType>& inputTasks, typename TaskType::value_type UB)
{
	typedef typename TaskType::value_type TimeType;

	TimeType Cmax;
	std::vector<TaskType> bestPermutation;


	auto U = schrage(inputTasks);

	if (U.Cmax < UB) {
		UB = U.Cmax;
		bestPermutation = U.taskPermutation;
	}

	
	auto b = findB(U);
	std::cout << b-U.taskPermutation.begin() << std::endl;
	auto a = findA(U, b);
	std::cout << a-U.taskPermutation.begin() << std::endl;
	auto c = findC(U.taskPermutation, b, a);
	std::cout << c-U.taskPermutation.begin() << std::endl;
	
	auto rPrim = std::min_element(c+1, U.taskPermutation.end(), CompareByR<TaskType>())->r;
	auto qPrim = std::min_element(c+1, U.taskPermutation.end(), CompareByQ<TaskType>())->q;
	TimeType pPrim = 0;
	for (auto it = c + 1; it <= b; ++it) {
		pPrim += it->p;
	}
	
	auto oldR = c->r;
	c->r = std::max(c->r, rPrim + pPrim);
	
	auto L = schrage_pre(U.taskPermutation);
	if (L.Cmax < U.Cmax) {
		L = carlier(L.taskPermutation, UB);
	}
	c->r = oldR;
	
	auto oldQ = c->q;
	c->q = std::max(c->q, qPrim +rPrim);
	if (L.Cmax < U.Cmax) {
		carlier(L.taskPermutation, UB);
	}
	
	c->q = oldQ;
	L = schrage_pre(L.taskPermutation);
	return RetType<TaskType> {std::move(bestPermutation), std::move(L.Cmax)};
}

#endif //ALGORITHMS_H
