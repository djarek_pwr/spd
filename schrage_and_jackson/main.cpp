#include <iostream>
#include "algorithms.hpp"

int main(int argc, char **argv) {
	auto jacksonLoader = [](auto& input, auto& task) {input >> task.r >> task.p;};
	std::string fileName, algorithm = "JACK";
	
	//auto tasks = loadTasks("JACK8.DAT");
	if (argc != 3) {
		fileName = "/home/damian/projects/SPD/testfiles/CARLIER/in/SCHRAGE2.DAT";
		algorithm = "CARLIER";
	} else {
		fileName = argv[1];
		algorithm = argv[2];
	}

	std::vector<Task> tasks;
	
	if (algorithm == "JACK") {
		tasks = loadTasks(fileName, jacksonLoader);
	} else {
		tasks = loadTasks(fileName);
	}
	
	RetType<Task> ret;
	
	if (algorithm == "JACK") {
		ret = jackson(tasks);
	} else if (algorithm == "SCHRAGE") {
		ret = schrage(tasks);
	} else if (algorithm == "SCHRAGE_PRE") {
		ret = schrage_pre(tasks);
	} else if (algorithm == "CARLIER") {
		auto UB = std::numeric_limits<Task::value_type>::max();
		ret = carlier(tasks, UB);
	}
	std::cout << ret.Cmax << std::endl;
	return 0;
}
