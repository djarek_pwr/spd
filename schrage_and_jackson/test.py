#!/usr/bin/python3
import os
from subprocess import check_output

def launch_SPD_app(fileName, algorithm):
  out = check_output(['build/spd', fileName, algorithm]).decode("utf-8")
  return out[:-1]

def load_out_file(fileName):
  with open(fileName, 'r') as f:
    return f.read()

def run_tests(src, algorithm):
  error = False
  in_files = sorted(os.listdir(os.path.join(src,algorithm,'in')))
  out_files = sorted(os.listdir(os.path.join(src,algorithm,'out')))
  if len(in_files) != len(out_files):
    raise RuntimeError
  i = 0
  for in_file in in_files:
    in_file_path = os.path.join(src,algorithm,'in', in_file)
    out_file_path = os.path.join(src,algorithm,'out', out_files[i]) 
    result = launch_SPD_app(in_file_path, algorithm)
    expected = load_out_file(out_file_path)
    if (result != expected):
      print("ERROR: result: " + result + " != expected: " + expected)
      error = True
    else:
      print("PASS: " + result)
    i += 1
  return error
    
if __name__ == '__main__':
  algorithms = ['JACK', 'SCHRAGE', 'SCHRAGE_PRE', 'CARLIER']
  error = False
  for algorithm in algorithms:
    print(algorithm + ': ')
    if run_tests('testfiles', algorithm):
      error = True
    print('\n')
  if not error:
    print('\nAll tests passed')
  else:
    print("\n FAIL")